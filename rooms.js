// a makeshift hash map. Room objects are assigned to room[] where their id is their index
// roomList[] list a list of all the room ids in existence
// this makes room CRUD functions O(1), and room counting easy.
var rooms = [],
	roomList = [];

function Room (id, name, listener) {
	this.id = id;
	this.name = name;
	this.users = [];
	this.queue = [];
	this.ttl = 0; // the setTimeout() for a rooms deletion after it has emptied (leaves leeway for accidental navigation or refresh)
	
	// keeps track of when the last song started which helps users stay relatively synced.
	this.updateLastSongStartedAt = function () {
		if (this.queue[0] && !this.queue[0].startedAt) {
			this.queue[0].startedAt = new Date().getTime();
		}
	}
}

// get all rooms (never more than 100)
exports.getAll = function () {
	var list = [];
	for (var i = 0; i < roomList.length; i++)
		list.push(rooms[roomList[i]]);
	return list;
}

// get room by id
exports.get = function (id) {
	return (rooms[id] ? rooms[id] : false);
}

// create a new room with a given name
exports.addRoom = function (name) {
	if	(roomList.length >= 100)
		return false;
	var id;
	do {
		id = new Date().getTime(); // figured this app would never be busy enough for this to be a problem
	} while (rooms[id]);
	rooms[id] = new Room(id, name);
	roomList.push(id);
	return rooms[id];
}

// delete room by id
exports.removeRoom = function (id) {
	rooms[id] = undefined;
	var i = roomList.indexOf(id);
	if (i == -1) return;
	else roomList.splice(i, 1);
}

// == user / room interaction == //

// user joins room
exports.userJoinRoom = function (roomId, userId) {
	var room = this.get(roomId);
	if (!room) return false;
	clearTimeout(room.ttl);
	room.users.push(userId);	
}

// user leaves room - an empty room is left up for 10 seconds in case of single user page refresh
exports.userLeaveRoom = function (roomId, userId) {
	var me = this;
	var room = me.get(roomId);
	if (!room) return false;
	if (room.users.length == 1) {
		room.ttl = setTimeout(function () {
			me.removeRoom(roomId);
		}, 10000);
		room.users = [];
		return;
	}
	for (var q = 0; q < room.queue.length; q++) {  // remove all user's 'done' votes (won't be reinstated if refresh)
		var d = room.queue[q].done.indexOf(userId);
		if (d == -1) continue;
		else room.queue[q].done.splice(d, 1);
	}
	var i = room.users.indexOf(userId);
	if (i > -1) room.users.splice(i, 1);
	return room.queue;
}

// user adds song to queue
exports.enqueueSong = function (roomId, song) {
	var room = this.get(roomId);
	if (!room) return false;
	if (room.queue.length >= 10) {
		return false;
	}
	song.done = [];
	room.queue.push(song);
	if (room.queue.length == 1) {
		room.updateLastSongStartedAt();
	}
	return room.queue;
}

// remove song
exports.removeSong = function (roomId, videoId) {
	var room = this.get(roomId);
	if (!room) return false;
	for (var i = 0; i < room.queue.length; i++) {
		if (room.queue[i].id == videoId) {
			room.queue.splice(i, 1);
			room.updateLastSongStartedAt();
			return room.queue;
		}
	}
}

// a user is 'done' with a song if it has either finished playing, or the chosen to vote it down
// a song is removed when more than 50% of the users in a room are donw with it
exports.done = function (roomId, videoId, userId) {
	var room = this.get(roomId);
	if (!room) return false;
	var song;
	for (var i = 0; i < room.queue.length; i++) {
		if (room.queue[i].id == videoId) {
			song = room.queue[i];
			break;
		}
	}
	if (!song) return;
	var index = song.done.indexOf(userId);
	if (index == -1) song.done.push(userId);
	if (song.done.length / room.users.length > 0.5) {
		this.removeSong (roomId, videoId);
	}
	return room.queue;
}
