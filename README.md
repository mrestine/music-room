### What is it? ###

It's a simple, anonymous way for uses to share their music preferences with YouTube-powered jukeboxes.

### What is used? ###

This started as an experiment so I could explore node.js / express / sockets.  Now I have it up at http://music-rooms.herokuapp.com (because it's free).  Not enough concurrency to require a database

### How do I get set up? ###

* get node.js
* change the location of the socket connection for the client (public/js/rooms.js) to your instance
* dependencies are included because this is the same repository as what's published on heroku. If you want to make sure, navigate to the app directory in the command line and run 'npm install'
* run 'node server.js' to start it

### Who do I talk to? ###

* developer - Matt Restine - matthew.restine@gmail.com