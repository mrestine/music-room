// ajax call for the list of rooms, draws them on the page
function refreshList () {
	$.ajax({
		method: 'get',
		url: '/refreshList',
		success: function (data) {
			$('#room-list-wrap').html(data);
			$('#create-toggle').click(function (ev) {
				$('#newroom-form').slideToggle('fast');
				ev.stopPropagation();
				$('#newroom-form-name-field').val('').focus();
			});
			$('#newroom-form').click(function (ev) {
				ev.stopPropagation();
			});
		}
	});
}

$(function () {
	// refresh the rooms every 10 seconds
	refreshList();
	setInterval(function () {refreshList()}, 10000);
	
	// new room form click listener
	$(window).click(function (ev) {
		$('#newroom-form').slideUp('fast');
	});
});