var socket = io.connect('http://music-rooms.herokuapp.com/', {query: "room=" + roomId, 'sync disconnect on unload': true}),
	room = {name: '', queue: [], userCount: 0},
	me, // socketId (socket.socket.socketId) -- uniquely identifies the user
	playing = '',  // YouTube ID of the playing song
	player = null,  // the YouTube player object
	ytReadyCallbacks = [],  // a list of functions that need to wait for the YouTube player to be ready
	first = true,  // true if a song has not been played yet
	durationInterval;  // the setInterval() result for drawing the song progress bar 

$(function () {

	// == all the socket listeners == //
	
	socket.on('connected', function () {  // user connected to server
		
		socket.emit('joinroom', {roomId: roomId});
		me = socket.socket.sessionid;
		
	}).on('joined', function (data) {  // initial round of room data
		
		data = data.room;
		room.name = data.name;
		room.queue = data.queue;
		room.userCount = data.users.length;
		$('#room-name').text(room.name);
		$('#user-count').text(room.userCount + ' user(s)');
		updatePlayer();
		updateQueueEl();
		$('#loading-overlay').remove();
		
	}).on('userjoined', function (data) {  // another user joined the room
		
		$('#user-count').text(++room.userCount + ' user(s)');
		
	}).on('userleft', function (data) {  // a user left the room 
		
		$('#user-count').text(--room.userCount + ' user(s)');
		
	}).on('queueupdate', function (data) {  // any kind of queue update
		
		room.queue = data.queue;
		updatePlayer();
		updateQueueEl();
		
	}).on('error', function (data) {  // fancy schmancy error handling
		
		alert('an unknown error occurred');
		console.log(data);
		
	});
	
	// volume button click listeners
	$('#volume-toggle').click(function (ev) {
		$('#volume-wrap').slideToggle('fast');
		ev.stopPropagation();
	});
	$(window).click(function (ev) {
		$('#volume-wrap').slideUp('fast');
	});
	
});

// called when the YouTube player is ready
function onYouTubePlayerReady () {
	
	player = $('#ytplayer')[0];
	player.setVolume($('#volume').val());
	player.addEventListener("onStateChange", "onStateChange");
	
	$('#volume').change(function (ev) {
		player.setVolume($('#volume').val());
	});	
	
	// call each of the callbacks that were waiting for the player to be ready
	for (var i = 0; i < ytReadyCallbacks.length; i++)
		ytReadyCallbacks[i]();
}

// when a song ends, tell the server that this user is done with the song
function onStateChange (ev) {
	if (ev == 0) 
		socket.emit('done', playing);
	else if (ev == 1)
		writeDuration();
}

// update the player
function updatePlayer () {
	
	// wait for player to be ready to update
	if (!player) {
		ytReadyCallbacks.push(updatePlayer);
		return;
	}
	
	// get the start offset of the first song in case this user joined mid-song
	var startOffset = 0;
	if (first) {
		startOffset = calculateOffset();
		first = false;
	}
	
	if (room.queue.length == 0 && playing) {  // if the queue emptied and a song is still playing
		
		player.stopVideo();
		clearInterval(durationInterval);
		
	} else if (room.queue[0] && room.queue[0].id != playing) {  // if there's a new song at the top of the queue
		
		player.loadVideoById(room.queue[0].id, startOffset, 'default');
		player.playVideo();
		playing = room.queue[0].id;
		durationInterval = setInterval(drawProgress, 50);
		
	}
}

// when a user enqueues a song from the search list
function enqueue(el) {
	$('#search-container').html('');
	$('#query').val('');
	socket.emit('enqueue', {
		id: $(el).attr('data'), 
		title: $(el).text(),
		thumb: $(el).children('img')[0].src
	});
}

// After the YouTube API loads, enable the search box
function loadAPIClientInterfaces() {
	gapi.client.load('youtube', 'v3', function() {
		$('#query').attr('disabled', false);
	});
}	

// Search for a specified string.
function search() {
	$('#search-container').html('');
	var q = $('#query').val();
	$('#search-container').addClass('searching');
	var request = gapi.client.youtube.search.list({
		type: 'video',
		q: q,
		maxResults: '10',
		key: 'AIzaSyC11XAzGCOQ0TJT17cFmYv5QKb7M7-1INQ',
		part: 'snippet'
	});
	
	// draw the search results
	request.execute(function(response) {
		$('#search-container').removeClass('searching');
		var result = response.items;
		for (var i = 0; i < result.length; i++) {
			$('#search-container').append(
				'<div class="result-row" data="' + result[i].id.videoId + '" onclick="enqueue(this)">' + 
				'<img src="' + result[i].snippet.thumbnails.default.url + '"></img>' + 
				result[i].snippet.title + '</div><br />'
			);
		}
	});
}

// Delay automatic search execution
var searchDelay;
$('#query').keyup(function () {
	clearTimeout(searchDelay);
	if ($('#query').val().length > 2)
		searchDelay = setTimeout(search, 750);
});

// draws the queue status and list
function updateQueueEl () {
	$('.queue-row').remove();
	
	// empty queue
	if (room.queue.length == 0) {
		$('#queue-state').text('queue is empty - add something by searching');
		return;
	}
	
	// full queue
	if (room.queue.length >= 10) {
		$('#query').attr('disabled', 'disabled');
	} else {
		$('#query').attr('disabled', false);
	}
	
	// non-empty queue
	$('#queue-state').text('now playing:');
	var firstClass = ' queue-first';
	for (var i = 0; i < room.queue.length; i++) {
		
		// draw the done button, grayed out or not
		var clickedClassStr = '';
		var clickListenerStr = ' onclick="done(this)" ';
		if (room.queue[i].done.indexOf(me) > -1) {
			clickedClassStr = ' clicked';
			clickListenerStr = '';
		}
		
		// draw the queue
		$('#queue-wrap').append(
			'<div class="queue-row' + firstClass + '" data="' + room.queue[i].id + '" >' + 
			'<img class="queue-thumb" src="' + room.queue[i].thumb + '"></img>' +
			room.queue[i].title + '<div class="queue-done' + clickedClassStr + '"' + clickListenerStr + '">[ ' + 
			room.queue[i].done.length +' ]</div></div>'
		);
		firstClass = '';
	}
	
	// draw the duration of the playing song
	if (room.queue && room.queue.length > 0 && player && player.getPlayerState() == 1)
		writeDuration();
}

// sends the signal down the pipe when the user is done with a song
function done (el) {
	var videoId = $(el).parent().attr('data');
	socket.emit('done', videoId);
}

// calculates the time offset in seconds to start the video so it's reasonably synched with other users
function calculateOffset () {
	if (room.queue && !room.queue[0]) return;
	var milliDiff = (new Date().getTime()) - room.queue[0].startedAt;
	return milliDiff / 1000;	
}

// draws an hr along the bottom of the playing song to indicate progress
function drawProgress () {
	var hr = $('.queue-first hr');
	if (!hr[0]) {
		$('.queue-first').append('<hr noshade width="0%" size="2">');
		hr = $('.queue-first hr');
	}
	var width = 100*(player.getCurrentTime() / player.getDuration());
	if (width > 100) width = 100;
	hr.attr('width', width + '%');
}

// format the duration of the video into soemthing more readable 
function writeDuration () {
	var duration = player.getDuration().toFixed(0);
	var hours = Math.floor(duration / 3600);
    var minutes = Math.floor((duration - (hours * 3600)) / 60);
    var seconds = duration - (hours * 3600) - (minutes * 60);

	if (hours == 0) hours = '';
	else hours += ':';
    if (minutes < 10 && hours != '') minutes = '0' + minutes;
    if (seconds < 10) seconds = '0' + seconds;
    var timeStr = hours + minutes + ':' + seconds;
	
	$('.queue-first').append('<div class="duration">' + timeStr + '</div>');
}
