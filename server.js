var express = require('express'),
	http = require('http'),
	app = express(),
	server = http.createServer(app),
	io = require('socket.io').listen(server),
	rooms = require('./rooms'),
	port = process.env.PORT || 3000;

	
app.configure(function(){
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.static(__dirname + '/public'));
	app.use(app.router);
});

// / - home page
app.get('/', function (req, res) {
	var roomList = rooms.getAll();
	res.render('lobby', {list: roomList});
	return;
});

// /refreshList gets the list of rooms
app.get('/refreshList', function (req, res) {
	var roomList = rooms.getAll();
	res.render('roomList', {list: roomList});
	return;
});

// /newroom creates a new room if there's not too many already
app.post('/newroom', function (req, res) {
	var name = req.body.name || "a room";
	var room = rooms.addRoom(name);
	if (!room) {
		res.send('too many rooms');
		return;
	}	
	res.redirect('/' + room.id);
	return;
});

// /:id brings the user to the room with that id
app.get('/:id', function (req, res) {
	var room = rooms.get(req.params.id);
	if (!room) {
		res.send('404');
		return;			
	}
	res.render('room', {room: room});
	return;
});

server.listen(port);

// == all the socket listeners (not enough for another file) == //

io.sockets.on('connection', function (socket) { // create listeners for each socket on connect
	var roomId;
	socket.emit('connected');
	
	// user joins room -- send back room info and broadcast to room that a user joined 
	socket.on('joinroom', function (data) {
		rooms.userJoinRoom(data.roomId, socket.id);
		roomId = data.roomId;
		socket.join(roomId);
		socket.emit('joined', {room: rooms.get(roomId)});
		socket.broadcast.to(roomId).emit('userjoined');		
	});
	
	// user leaves room -- broadcast to room that user left and queue update
	socket.on('disconnect', function () {
		socket.broadcast.to(roomId).emit('userleft');
		socket.leave(roomId);
		var queue = rooms.userLeaveRoom(roomId, socket.id);
		io.sockets.in(roomId).emit('queueupdate', {queue: queue});
	});
	
	// user enqueues song -- broadcast the queue update
	socket.on('enqueue', function (data) {
		var queue = rooms.enqueueSong(roomId, data);
		if (!queue) {
			socket.emit('error', 'queue full');
		};
		io.sockets.in(roomId).emit('queueupdate', {queue: queue});
	});
	
	// user done with song -- broadcast queue update
	socket.on('done', function (data) {
		var queue = rooms.done(roomId, data, socket.id);
		io.sockets.in(roomId).emit('queueupdate', {queue: queue});
	});
});
